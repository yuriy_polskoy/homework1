package ifElse;

public class Task2 {

    public static void main(String[] args) {

        int a = 3;
        int b = 3;
        int result;

        if (a % 2 == 0) result = a + b; else result = a * b;

        System.out.println(result);

    }
}
