package ifElse;

public class Task3 {

    public static void main(String[] args) {

        int a = 1;
        int b = -2;
        int c = 3;
        int result = 0;

        if (a > 0) result += a;
        if (b > 0) result += b;
        if (c > 0) result += c;

        System.out.println(result);


    }

}
