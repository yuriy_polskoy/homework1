package ifElse;

public class Task5 {

    public static void main(String[] args) {

        int ratio = 66;

        char mark = 'F';

        if (ratio > 19) mark = 'E';
        if (ratio > 39) mark = 'D';
        if (ratio > 59) mark = 'C';
        if (ratio > 74) mark = 'B';
        if (ratio > 89) mark = 'A';

        System.out.println("Оценка студента - " + mark);

    }

}
